package aed;

// Todos los tipos de datos "Comparables" tienen el método compareTo()
// elem1.compareTo(elem2) devuelve un entero. Si es mayor a 0, entonces elem1 > elem2
public class ABB<T extends Comparable<T>> implements Conjunto<T> {
    private Nodo _raiz;
    private int _cardinal;
    private T _min;
    private T _max;
    // Agregar atributos privados del Conjunto

    private class Nodo {
        T valor;
        Nodo izq;
        Nodo der;
        Nodo padre;
        Nodo (T v){
            valor = v;
            izq = null;
            der = null;
            padre = null;
        }
        // Agregar atributos privados del Nodo

        // Crear Constructor del nodo
    }

    public ABB() {
        _raiz = null;
        _cardinal = 0;
    }

    public int cardinal() {
        return _cardinal;
    }

    public T minimo(){
        Nodo actual = _raiz;
        while (actual.izq != null){
            actual= actual.izq;
        }
        _min = actual.valor;
        return actual.valor;
    }

    public T maximo(){
        Nodo actual = _raiz;
        while (actual.der != null){
            actual= actual.der;
        }
        _max = actual.valor;
        return actual.valor;
    }

    public void insertar(T elem){
        Nodo actual = _raiz;
        Nodo nuevo = new Nodo(elem);
        if (_raiz == null){
            _raiz = nuevo;
            nuevo.izq = null;
            nuevo.der = null;
            _cardinal += 1;
        }
        else{
            while(actual != null){
            if(elem.compareTo(actual.valor) < 0){
                if(actual.izq == null){
                    actual.izq = nuevo;
                    nuevo.padre = actual;
                    _cardinal += 1;
                    break;
                }
                else{
                    actual = actual.izq;
                }
            }
            else if(elem.compareTo(actual.valor) > 0){
                if(actual.der == null){
                    actual.der = nuevo;
                    nuevo.padre = actual;
                    _cardinal += 1;
                    break;
                }
                else{
                    actual = actual.der;
                }
            }
            else{
                break;
                }
            }
        }
    }

    public boolean pertenece(T elem){
        Nodo actual = _raiz;
        while(actual != null){
            if(elem.compareTo(actual.valor) == 0){
                return true;
            }
            else{
                if(elem.compareTo(actual.valor) < 0){
                    actual = actual.izq;
                }
                else{
                    actual = actual.der;
                }
            }
        }
        return false;
    }

    public void eliminar(T elem){
        Nodo actual = _raiz;
        Nodo padre = null;
        _cardinal--;
         while(actual != null){
            if(elem.compareTo(actual.valor) == 0){
                if(actual.izq == null && actual.der == null){
                    if(padre == null){
                    _raiz = null;
                }
                else{
                    if(padre.izq == actual){
                        padre.izq = null;
                    }
                    else{
                        padre.der = null;
                    }
                }
            }
            else{
                if(actual.izq != null && actual.der == null){
                    if(padre == null){
                        _raiz = actual.izq;
                    }
                    else{
                        if(padre.izq == actual){
                            padre.izq = actual.izq;
                        }
                        else{
                            padre.der = actual.izq;
                        }
                    }
                }
                else{
                    if(actual.izq == null && actual.der != null){
                        if(padre == null){
                            _raiz = actual.der;
                        }
                        else{
                            if(padre.izq == actual){
                                padre.izq = actual.der;
                            }
                            else{
                                padre.der = actual.der;
                            }
                        }
                    }
                    else{
                        Nodo predecesor = actual.izq;
                        while(predecesor.der != null){
                            predecesor = predecesor.der;
                        }
                        T valorPredecesor = predecesor.valor;
                        eliminar(valorPredecesor);
                        actual.valor = valorPredecesor;
                        _cardinal++;
                    }
                }
            }
            break;
            }
            else{
                padre = actual;
                if(elem.compareTo(actual.valor) < 0){
                    actual = actual.izq;
                }
                else{
                    actual = actual.der;
                }
            }
        }
    }

    public StringBuffer inOrder(Nodo nodo) {
        StringBuffer lista = new StringBuffer("");
        if (nodo != null){
            lista.append(inOrder(nodo.izq));
            if(nodo.izq != null){
                lista.append(",");
            }
            lista.append(nodo.valor);
            if (nodo.der != null){
                lista.append(",");
            }
            lista.append(inOrder(nodo.der));
        }
        return lista;
    }

    public String toString(){
        StringBuffer lista = new StringBuffer("{");
        lista.append(inOrder(_raiz));
        lista.append("}");
        return lista.toString();
    }

    private class ABB_Iterador implements Iterador<T> {
        private Nodo _actual;
        int posicion;
        ABB_Iterador(){
            posicion = 0;
        }
        
        public Nodo minimoIterado(Nodo raiz){
        Nodo _actual = _raiz;
        while (_actual.izq != null){
            _actual= _actual.izq;
        }
        return _actual;
        }
        public boolean haySiguiente() {            
            return posicion < _cardinal;

        }
    
        public T siguiente() {
            if(posicion == 0){
                posicion ++;
                _actual = minimoIterado(_raiz);
                return _actual.valor;
            }
            else{
            Nodo res = _actual;
            if (_actual.der != null){
                _actual = _actual.der;
                while(_actual.izq != null){
                    _actual = _actual.izq;
                }
                res = _actual;
            }
            else{
                while(_actual.padre != null && (_actual.padre.valor).compareTo(_actual.valor) < 0){
                    _actual = _actual.padre;
                }
                _actual = _actual.padre;
                res = _actual;
                }
                posicion ++;
                return res.valor;
            }
        }
    }

    public Iterador<T> iterador() {
        return new ABB_Iterador();
    }

}
