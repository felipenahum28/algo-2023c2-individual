package aed;

public class InternetToolkit {
    public InternetToolkit() {
    }

    public Fragment[] tcpReorder(Fragment[] fragments) {
        // IMPLEMENTAR
        for(int i = 1; i < fragments.length; i++){
            int j = i;
            Fragment elem = fragments[i];
            while((j > 0) && (fragments[j-1].compareTo(elem) > 0)){
                fragments[j] = fragments[j-1];
                j--;
            }
            fragments[j] = elem;
        }
        return fragments;
    }

    public Router[] kTopRouters(Router[] routers, int k, int umbral) {
        // IMPLEMENTAR
        Router[] arribaUmbral = new Router[routers.length];
        int traficosValidos = 0;
        for(int i = 0; i < routers.length; i++){
            if(routers[i].getTrafico() >= umbral){
                arribaUmbral[traficosValidos] = routers[i];
                traficosValidos++;
            }
        }
        maxHeap<Router> topRouters = new maxHeap<Router>(arribaUmbral,traficosValidos);
        Router[] routersMaximos = new Router[k];
        int i = 0;
        int routerPasados = 0;
        while(i < traficosValidos && routerPasados < k){
             routersMaximos[i] = topRouters.desapilar();
             i++;
             routerPasados++; 
        }
        return routersMaximos;
    }

    public IPv4Address[] sortIPv4(String[] ipv4) {
        // IMPLEMENTAR
        IPv4Address[] direcciones = new IPv4Address[ipv4.length];
        for(int i = 0; i < ipv4.length; i++){
            IPv4Address direccion = new IPv4Address(ipv4[i]);
            direcciones[i] = direccion;
        }
        for(int j = 3; j >= 0; j--){
            for (int z = 0; z < ipv4.length - 1; z++){
                for(int i = 0; i < ipv4.length - z - 1; i++)
                    if(direcciones[i].getOctet(j) > direcciones[i+1].getOctet(j)){
                        IPv4Address actual = direcciones[i];
                        direcciones[i] = direcciones[i+1];
                        direcciones[i+1] = actual;
                    }
            }
        }
        return direcciones;
    }

}
