package aed;

public class ListaEnlazada<T> implements Secuencia<T> {
    private int size;
    private Nodo primero;
    private Nodo ultimo;
    // Completar atributos privados

    private class Nodo {
        Nodo sig;
        Nodo ant;
        T valor;
        Nodo(T v) {
            valor = v;
            sig = null;
            ant = null;
        }
        // Completar
    }

    public ListaEnlazada() {
        primero = null;
        ultimo = null;
        size = 0;
    }

    public int longitud() {
        return size;
    }

    public void agregarAdelante(T elem) {
        Nodo nuevo = new Nodo(elem);
        nuevo.sig = primero;
        primero = nuevo;

        size++;
    }

    public void agregarAtras(T elem) {
        Nodo nuevo = new Nodo(elem);
        if (primero == null){
            primero = nuevo;
        }
        else{
            Nodo actual = primero;
            while(actual.sig != null){
                actual = actual.sig;
            }
            actual.sig = nuevo;
            nuevo.ant = ultimo;
        }
        ultimo = nuevo;
        size++;
    } 

    public T obtener(int i) {
        Nodo actual = primero;
            for(int j = 0; j < i; j++){
                actual = actual.sig;
            }
        return actual.valor;
    }

    public void eliminar(int i) {
        Nodo actual = primero;
        if (i == 0){
            primero = primero.sig;
        }
        else{
            for(int j = 1; j < i; j++ ){
                actual = actual.sig;
            }
            actual.sig = actual.sig.sig;
        }
        size--;
    }

    public void modificarPosicion(int indice, T elem) {
        Nodo actual = primero;
        for(int j = 0; j < indice; j++){
            actual = actual.sig;
        }
        actual.valor = elem;

    }

    public ListaEnlazada<T> copiar() {
       ListaEnlazada<T> copia = new ListaEnlazada<>();
       Nodo actual = primero;
       while(actual != null){
            copia.agregarAtras(actual.valor);
            actual = actual.sig;
       }
    return copia;
    }

    public ListaEnlazada(ListaEnlazada<T> lista) {
        ListaEnlazada<T> copia = lista.copiar();
        size = lista.size;
        primero = copia.primero;
        ultimo = copia.ultimo;


    }
    
    @Override
    public String toString() {
        String res = "";
        Nodo actual = primero;
        while(actual.sig != null){
            res = res + actual.valor + ", ";
            actual = actual.sig;
            }
        res = res + actual.valor;
        return "[" + res + "]";
        
    }

    private class ListaIterador implements Iterador<T> {
        int posicion;
        ListaIterador(){
            posicion = 0;
        }

        public boolean haySiguiente() {
	       return posicion != size;
        }
        
        public boolean hayAnterior() {
	        return posicion - 1 >= 0;
        }

        public T siguiente() {
	        int i = posicion;
            posicion = posicion + 1;
            return obtener(i);
        }
        

        public T anterior() {
            posicion = posicion - 1;
            return obtener(posicion);
        }
    }

    public Iterador<T> iterador() {
	    return new ListaIterador();
    }

}
