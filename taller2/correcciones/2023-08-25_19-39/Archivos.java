package aed;

import java.io.PrintStream;
import java.util.Scanner;

class Archivos {
    float[] leerVector(Scanner entrada, int largo) {
        float[] res = new float[largo];
        for (int i = 0; i < largo; i++){
            res[i] = entrada.nextFloat();
        }
        return res;
    }

    float[][] leerMatriz(Scanner entrada, int filas, int columnas) {
        float[][] mat = new float[filas][columnas];
        for (int i = 0; i < filas; i++){
            mat[i]= leerVector(entrada, columnas);
        }
        return mat;
    }

    void imprimirPiramide(PrintStream salida, int alto) {
        for(int l = 0 ; l < alto; l++){
            for(int i = 0;i < 2*alto -1;i++ ){
                if((i < alto -1 -l) || (i > alto -1 +l)){
                    salida.print(" ");
                }else {
                    salida.print("*");
                }
            } salida.println(); 
        }
    }
}
