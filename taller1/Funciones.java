package aed;

class Funciones {
    int cuadrado(int x) {
        int res = x * x;
        return res;
    }

    double distancia(double x, double y) {
        double res = Math.sqrt((x * x) + (y * y));
        return res;
    }

    boolean esPar(int n) {
        return (n % 2 == 0);
            
    }

    boolean esBisiesto(int n) {
        return (n % 400 == 0) || ((n % 4 == 0) && (n % 100 != 0));
    }

    int factorialIterativo(int n) {
        int res = 1;
        if (n == 0 || n == 1) {
            res = 1;
        }
        for (int i = 1; i <= n; i++){
            res = i * res;
        }
        return res;
    }

    int factorialRecursivo(int n) {
        int res = 0;
        if (n == 0) {
            res = 1;
        } else {
            res = n * factorialRecursivo(n - 1);
        }
        return res;
    }

    boolean esPrimo(int n) {
        int res = 0;
        for (int i = 1; i <= n; i++){
            if ((n % i) == 0) {
                res = res + 1;
            } else {
                res = res + 0;
            }
        }
        if (res == 2) {
            return true;
        }
        return false;
    }

    int sumatoria(int[] numeros) {
        int res = 0;
        for (int i = 0; i < numeros.length; i++){
            res = res + numeros[i];
        }
        return res;
    }

    int busqueda(int[] numeros, int buscado) {
        for (int i = 0; i < numeros.length; i++){
            if (numeros[i] == buscado) {
                return i;
            }
        }
        return 0;
    }

    boolean tienePrimo(int[] numeros) {
        for (int i = 0; i < numeros.length; i++){
            if (esPrimo(numeros[i]) == true){
                return true;
            }
        }
        return false;
    }

    boolean todosPares(int[] numeros) {
        boolean res = true;
        for (int i = 0; i < numeros.length; i++){
            if ((esPar(numeros[i])) && res){
                res = true;
            } else {
                res = false;
            }
        }
        return res;
    }

    boolean esPrefijo(String s1, String s2) {
        boolean res = true;
        if (s1.length() > s2.length()){
            return false;
        }
        for (int i = 0; i < s1.length(); i++){
            if (s1.charAt(i) == s2.charAt(i) && res){
                res = true;
            } else {
                res = false;
            }
        }
        return res;
    }

    boolean esSufijo(String s1, String s2) {
        return (esPrefijo(invertir(s1), invertir(s2)));

    }
    String invertir(String s1) {
        String res = "";
        for (int i = s1.length()-1; i >= 0; i--){
            res = res + s1.charAt(i);
        }
        return res;

    }
}
